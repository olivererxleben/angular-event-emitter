import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css']
})
export class MyComponent implements OnInit {

  @Output() delay = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {

  }

  onMouseOver(time: number) {
    console.log('mouseover');

    setTimeout( _ => {

      this.delay.emit();
    }, time);
  }

}
